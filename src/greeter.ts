import { Observable } from "@reactivex/rxjs";

export class Greeter {
  private greeting: string;

  constructor(message: string) {
    this.greeting = message;
  }

  public greet() {
    return "Bonjour, " + this.greeting + "!";
  }

  public ping(): Observable<string> {
    return Observable.of("ping");
  }
};
