import { Katana, Ninja, Shuriken } from "./inverifyjs/implementations";
import { IThrowableWeapon, IWarrior, IWeapon, TYPES } from "./inverifyjs/interfaces";
import { Kernel } from "inversify";

let kernel = new Kernel();
kernel.bind<IWarrior>(TYPES.Warrior).to(Ninja);
kernel.bind<IWeapon>(TYPES.Weapon).to(Katana);
kernel.bind<IThrowableWeapon>(TYPES.ThrowableWeapon).to(Shuriken);

export default kernel;
