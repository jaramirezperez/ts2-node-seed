import { IThrowableWeapon, IWarrior, IWeapon, TYPES } from "./interfaces";
import { inject, injectable } from "inversify";
import "reflect-metadata";

@injectable()
class Katana implements IWeapon {
  public hit() {
    return "cut!";
  }
}

@injectable()
class Shuriken implements IThrowableWeapon {
  public throw() {
    return "hit!";
  }
}

@injectable()
class Ninja implements IWarrior {

  private katana: IWeapon;
  private shuriken: IThrowableWeapon;

  public constructor(
    @inject(TYPES.Weapon) katanaImpl: IWeapon,
    @inject(TYPES.ThrowableWeapon) shurikenImpl: IThrowableWeapon
  ) {
    this.katana = katanaImpl;
    this.shuriken = shurikenImpl;
  }

  public fight() { return this.katana.hit(); };
  public sneak() { return this.shuriken.throw(); };

}

export { Ninja, Katana, Shuriken };
