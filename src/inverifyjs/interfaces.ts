export interface IWarrior {
  fight(): string;
  sneak(): string;
}

export interface IWeapon {
  hit(): string;
}

export interface IThrowableWeapon {
  throw(): string;
}

export let TYPES = {
  ThrowableWeapon: Symbol("ThrowableWeapon"),
  Warrior: Symbol("Warrior"),
  Weapon: Symbol("Weapon"),
};
