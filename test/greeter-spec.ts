import { Greeter } from "../src/greeter";
import test from "ava";


test("my observable passing test", t => {
  t.plan(2);
  const greeter = new Greeter("test");
  greeter.ping().subscribe(
    x => { t.is(x, "ping"); },
    err => { t.fail(err); },
    () => {
      t.pass();
    }
  );
});


test("my passing test", t => {
    t.pass();
});
