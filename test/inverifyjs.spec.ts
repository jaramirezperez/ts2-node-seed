import { IWarrior, TYPES } from "../src/inverifyjs/interfaces";
import kernel from "../src/inversify.config";
import test from "ava";

test("inverifyjs test", t => {
  const ninja = kernel.get<IWarrior>(TYPES.Warrior);
  t.is(ninja.fight(), "cut!");
  t.is(ninja.sneak(), "hit!");
});
