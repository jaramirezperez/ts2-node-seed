# Using this module in other modules

Here is a quick example of how this module can be used in other modules. The [TypeScript Module Resolution Logic](https://www.typescriptlang.org/docs/handbook/module-resolution.html) makes it quite easy. The file `src/index.ts` acts as an aggregator of all the functionality in this module. It imports from other files and re-exports to provide a unified interface for this module. The _package.json_ file contains `main` attribute that points to the generated `lib/index.js` file and `typings` attribute that points to the generated `lib/index.d.ts` file.

> If you are planning to have code in multiple files (which is quite natural for a NodeJS module) that users can import, make sure you update `src/index.ts` file appropriately.

Now assuming you have published this amazing module to _npm_ with the name `my-amazing-lib`, and installed it in the module in which you need it -

- To use the `Greeter` class in a TypeScript file using RxJs -

```ts
import { Greeter } from "my-amazing-lib";

const greeter = new Greeter("World!");
greeter.greet();

...

test/greeter-spec.ts

test("my observable passing test", t => {
  t.plan(2);
  const greeter = new Greeter("test");
  greeter.ping().subscribe(
    x => { t.is(x, "ping"); },
    err => { t.fail(err); },
    () => {
      t.pass();
    }
  );
});

```

- To use the `Greeter` class in a JavaScript file -

```js
const Greeter = require('my-amazing-lib').Greeter;

const greeter = new Greeter('World!');
greeter.greet();
```

# But... this is a hack... use the amazing IOC with inversify
```
...

test/inversifyjs-spec.ts

test("inverifyjs test", t => {
  const ninja = kernel.get<IWarrior>(TYPES.Warrior);
  t.is(ninja.fight(), "cut!");
  t.is(ninja.sneak(), "hit!");
});

```
